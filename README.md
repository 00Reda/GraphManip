# GraphManip
## Exemple :

on considere le graph suivant : 


![alt text](https://github.com/00Reda/GraphManip/blob/master/img/graph.jpeg)

on consulte la page suivante : https://fstt.000webhostapp.com/GraphManip/index.html
o donne le nombre de noeuds et on click sur le button : visualisation .

on est redirigé vers la page suivante : https://fstt.000webhostapp.com/GraphManip/visualisation.html afin de donner la matrice d'adjacence

![alt text](https://github.com/00Reda/GraphManip/blob/master/img/matrice.PNG)

en suite on click sur le button visualisation pour voir le graph comme suit :

![alt text](https://github.com/00Reda/GraphManip/blob/master/img/Capture.PNG)

et donc vous pouvez choisir parmis les algorithmes suivants :

![alt text](https://github.com/00Reda/GraphManip/blob/master/img/algo.PNG)
